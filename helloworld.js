// HELLO WORLD
//console.log('Hello world');

/*
// REVERSE
function reverse(string) {
    let reversestring = '';
    for(var i = string.length-1; i>=0; i--){
        reversestring = reversestring + string[i];
    }
    return reversestring
}

reverse("Propulsion Academy"); // "ymedacA noisluporP"
//reverse("Hello") // "olleH"
//reverse("abcd") // "dcba"

*/

// FACTORIAL
/*
function factorial(n) {
    if(n === 0){
        return 1;
    } else if (n<0) {
        return "n must be > or = to 0";
    }
    else {
        return factorial(n-1)*n;
    } 
}

factorial(5); // 120
factorial(4);   // 24
factorial(0); // 1
factorial(-1); // "n must be > or = to 0"
*/


// LONGEST WORD
/*
function longest_word(sentence) {
    let words = sentence.split(" ");
    let longestWord = '';
    for(var i = words.length-1; i>=0; i--){
        if(words[i].length > longestWord.length){
            longestWord = words[i];
        }
    }
    return longestWord; 
}

longest_word("This is an amazing test") // "amazing"
longest_word("Laurent Colin") // "Laurent"
longest_word("Propulsion 123") // "Propulsion"
*/

//SUM NUMS
/*
function sum_nums(num) {
    let i = 0;
    let sum = 0;
    while(i <= num){
        sum = sum + i;
        i++;
    }
    return sum;
}

sum_nums(6) // 21
sum_nums(1) // 1
sum_nums(0) // 0
*/

//TIME CONVERSION
/*
function time_conversion(seconds) {
    let sec = seconds%60;
    let min = (seconds- sec)/60;
    if( sec < 10) { sec = '0' + sec;}
    if( min < 10){ min = '0'+ min;}
    return min + ":" + sec;
}

time_conversion(155) // "02:35"
time_conversion(61) // "01:01"
time_conversion(60) // "01:00"
time_conversion(59) // "00:59"
*/

// COUNT VOWELS
/*
function count_vowels(string) {
    string = string.toLowerCase();
    let count = 0;
    for(var i = string.length-1; i>=0; i--){
        if(string[i] === 'a' || string[i] === 'e' || string[i] === 'i' || string[i] === 'o' || string[i] === 'u' ){
            count++;
        }
    }
    return count;
}

count_vowels("alphabet") // 3
count_vowels("Propulsion Academy") // 7
count_vowels("AaaAa") // 5
count_vowels("fly") // 0
*/

// PALINDROME
/*
function palindrome(string) {
    string = string.toLowerCase();
    backwards = '';
    for(var i = string.length-1; i>=0; i--){
        backwards = backwards + string[i];
    }
    if(string === backwards){
        return true;
    } else {
        return false; 
    }
}

palindrome("ABBA") // true
palindrome("AbbA") // true
palindrome("abcd") // false
*/

// NEARBY AZ
/*
function nearby_az(string) {
    string = string.toLowerCase();
    let indexA = string.indexOf("a");
    let indexZ = string.indexOf("z");
    
    if(indexZ > indexA && indexZ <= indexA + 3){
        return true;
    } else {
        return false;
    }


}

nearby_az("abbbz") // false
nearby_az("abz") // true
nearby_az("abcz") // true
nearby_az("abba") // false
*/

// TWO SUM
/*
function two_sum(nums) {
    let twosum = [];
    for(var i = 0; i < nums.length; i++){
        for(var j = i; j < nums.length; j++){
            if(nums[i] + nums[j] === 0){
                twosum.push([i,j]);
            }
        }
    }
    if(twosum.length === 0){
        return null;
    } else {
        return twosum;
    }

}

two_sum([1, 3, -1, 5]) // [[0, 2]]
two_sum([1, 3, -1, 5, -3]) // [[0, 2], [1, 4]]
two_sum([1, 5, 3, -4]) // null
*/

// IS POWER OF TWO
/*
function is_power_of_two(num) {
    if(Math.log2(num)%1 == 0){
        return true;
    } else {
        return false;
    }
}

is_power_of_two(8) // true
is_power_of_two(16) // true
is_power_of_two(32) // true
is_power_of_two(12) // false
is_power_of_two(24) // false
*/


// REPEAT A STRING
/*
function repeat_string_num_times(str, num) {
    let newstr = '';
    if(num > 0){
        for(var i = 0; i < num; i++){
            newstr = newstr + str;
        }
        return newstr;
    } else{
        return '';
    }   
}

repeat_string_num_times("abc", 3) // 'abcabcabc'
repeat_string_num_times("abc", 1) // 'abc'
repeat_string_num_times("abc", 0) // ''
repeat_string_num_times("abc", -1) // ''
*/

// ADD ALL
/*
function add_all(arr) {
    let sum = 0;
    for(var i = arr[0]; i <= arr[1]; i++){
        sum = sum + i;
    }
    console.log(sum);
}

add_all([1, 4]) // 10
add_all([5, 10]) // 45
add_all([9, 10]) // 19
add_all([0, 0]) // 0
add_all([-1, 1]) // 0
*/

// IT IS TRUE
/*
function is_it_true(args) {
    if(args === true || args === false){
        return true;
    } else{
        return false;
    }
}

is_it_true(true) // true
is_it_true(false) // true
is_it_true('true') // false
is_it_true(1) // false
is_it_true('false') // false
*/

// LARGEST OF FOUR
/*
function largest_of_four(arr) {
    let largestOfFour = [];
    for(var i = 0; i < arr.length; i++){
        for(var j = 0; j < arr[i].length; j++){
            if(j === 0){
                largestOfFour.push(arr[i][0]);
            }
            if(arr[i][j] > largestOfFour[i]){
                largestOfFour[i] = arr[i][j];
            }
        }
    }
    return largestOfFour; 
}

largest_of_four([[13, 27, 18, 26], [4, 5, 1, 3], [32, 35, 37, 39], [1000, 1001, 857, 1]]) // [27,5,39,1001]
*/


// IS ANAGRAM
/*
function alphabet_order(str)
{
    return str.split('').sort().join('');
}


function isAnagram(test, original) {
    test = test.toLowerCase();
    original = original.toLowerCase();

    test = alphabet_order(test);
    original = alphabet_order(original);

    if(test === original){
        return true;
    } else {
        return false;
    }
};

isAnagram("foefet", "toffee") // true
isAnagram("Buckethead", "DeathCubeK") // true
isAnagram("Twoo", "WooT") // true
isAnagram("dumble", "bumble") // false
isAnagram("ound", "round") // false
isAnagram("apple", "pale") // false
*/